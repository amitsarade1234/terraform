resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}                       

resource "aws_internet_gateway" "igw-getway" {                           #igw
  vpc_id = aws_vpc.main.id
 }

resource "aws_vpc_attachment" "my_vpc_attachment" {
  vpc_id = aws_vpc.main.id
  internet_gateway_id = aws_internet_gateway.igw-getway.id
}
 
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/22"
  }

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.0.0/22"
  map_public_ip_on_launch = true
  }

resource "aws_default_route_table" "example" {                               #default_route_table_id
  default_route_table_id = aws_vpc.example.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-getway.id
  }
}
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.foo.id
  route_table_id = aws_route_table.bar.id
}